const { build } = require('esbuild');
const { nodeExternalsPlugin } = require('esbuild-node-externals');

const sharedConfig = {
  entryPoints: ['src/index.ts'],
  outfile: 'dist/index.js',
  bundle: true,
  minify: true,
  sourcemap: true,
  target: 'node18',
  plugins: [nodeExternalsPlugin()],
};

const CJS = {
  ...sharedConfig,
  platform: 'node', // for CJS
  outfile: 'dist/index.js',
};

const ESM = {
  ...sharedConfig,
  outfile: 'dist/index.esm.js',
  platform: 'neutral', // for ESM
  format: 'esm',
};

build(CJS).catch(() => process.exit(1));
build(ESM).catch(() => process.exit(1));
