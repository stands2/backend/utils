import { sum } from '../src';

import { describe, expect, it } from 'vitest';

describe('sum function', () => {
  it('adds 2 numbers', () => {
    expect(sum(1, 2)).toBe(3);
  });
});
